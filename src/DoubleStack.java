import java.util.LinkedList;

public class DoubleStack {

	public static void main(String[] argum) {
		// TODO!!! Your tests here!
	}

	/** Loon stacki */
	private LinkedList<Double> stack;

	/** Loon konstruktori */
	public DoubleStack() {
		stack = new LinkedList<Double>();

	}

	/**
	 * Loon exceptioni. Kasutasin ideid
	 * http://www.javapractices.com/topic/TopicAction.do?Id=71
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		DoubleStack rtn = new DoubleStack();
		rtn.stack = (LinkedList<Double>) stack.clone();
		return rtn;
	}

	/**
	 * Kontrollin kas stack on tyhi. Return true kui stack on tyhi
	 */
	public boolean stEmpty() {
		return (stack.isEmpty()) ? true : false;
	}

	/** Asetab elemendi stacki. */
	public void push(double a) {
		stack.push((double) a);
	}

	/** Toob esile elemendi stackist. */
	public double pop() {
		if (stack.isEmpty())
			throw new IllegalStateException ("Magasin tyhi");
		return stack.pop();
	}

	/**
	 * Aritmeetikatehe s magasini kahe pealmise elemendi vahel (tulemus pannakse
	 * uueks tipuks) Kasutasin magasini naidet -
	 * http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
	 */
	public void op(String s) {
		char op = s.charAt(0);
		double elem1 = stack.remove(1);
		double elem2 = stack.remove(0);
		switch (op) {
		case '+':
			push(elem1 + elem2);
			break;
		case '-':
			push(elem1 - elem2);
			break;
		case '*':
			push(elem1 * elem2);
			break;
		case '/':
			push(elem1 / elem2);
			break;
		default:
			throw new RuntimeException("Mittesobiv tehe '" + op + "' (sobivad tehted on: +, -, * ja /");
		}
	}

	/** Tipu lugemine eemaldamiseta */
	public double tos() {
		if (stack.isEmpty())
			throw new IllegalStateException ("Magasin tyhi");
		return stack.peek();
	}

	/**
	 * Kahe stacki vordsuse kindlaks tegemine. Kasutasin ideid:
	 * https://docs.oracle.com/javase/tutorial/java/IandI/objectclass.html
	 * http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
	 * http://www.java-samples.com/showtutorial.php?tutorialid=660
	 * 
	 */

	@Override
	
	public boolean equals(Object o) {
		DoubleStack dbst = (DoubleStack) o;

		if (stack.size() != dbst.stack.size())
			return false;

		int i = 0;
		for (double m : stack) {
			// System.out.println(c +" "+ ds.m.get(i));
			if (m != dbst.stack.get(i))
				return false;

			i++;
		}
		return true;
	}

	/**
	 * Teisendus s�naks (tipp l�pus) Kasutatud ideed -
	 * http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
	 * http://www.tutorialspoint.com/java/lang/stringbuffer_tostring.htm
	 */
	@Override
	public String toString() {
		StringBuffer rtn = new StringBuffer();
		for (int i = stack.size() - 1; i >= 0; i--) {
			rtn.append(stack.get(i));
			if (i > 0)
				rtn.append(' ');
		}
		return rtn.toString();
	}

	/**
	 * Aritmeetilise avaldise p��ratud poola kuju (sulgudeta postfikskuju,
	 * Reverse Polish Notation)
	 * Kasutasin ideed - http://introcs.cs.princeton.edu/java/43stack/
	 * http://www.java2s.com/Code/Java/Collections-Data-Structure/ReversePolishNotation.htm
	 * http://www.kirkwood.edu/pdf/uploaded/262/cs2f08_-_stacks2.pdf	
	 * http://math.hws.edu/javanotes/c3/s7.html 
	 */
	public static double interpret(String pol) {
		if (pol.length() == 0 || pol == null )
			throw new RuntimeException("Avaldis ei saa olla tyhi");
		DoubleStack stack = new DoubleStack();
		String[] elements = pol.trim().split("\\s+");
		double i = 0, o = 0;
		for (String element : elements) {
			try {
				stack.push(Double.valueOf(element));
				i++;
			} catch (NumberFormatException e) {
				if (!element.equals("+") && !element.equals("-") && !element.equals("*") && !element.equals("/"))
					throw new RuntimeException("Avaldis sisaldab valet tehet " + element + pol + " .");
				else if (stack.stEmpty())
					throw new RuntimeException("Tehe ei saa olla avaldise esimene element ");
				else if (stack.stack.size() < 2)
					throw new RuntimeException("Liiga v�he elemente stackis, et teostada tehet");
				stack.op(element);
				o++;
			}
		}
		if (i - 1 != o)
			throw new RuntimeException("Stack ei ole tasakaalus: tehted - " + o + "muutujad - " + i + " .");

		return stack.pop();
	}

}
